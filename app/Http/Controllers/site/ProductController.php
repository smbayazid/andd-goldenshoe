<?php

namespace App\Http\Controllers\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Products;
use DB;
use App\Product_Inventory;
use App\Order;
use Session;
class ProductController extends Controller
{
     public function products(){
     	$all_products = Products::all();
    	return view('site.products',compact('all_products'));
    }
    public function product_details(Request $request){
    	$id_product = $request->product_id;
    	$product = Products::find($id_product);
    	$inventories = Product_Inventory::where('product_id',$id_product )->get();
    	return view('site.product-details',compact('product','inventories'));
    }
    public function addToCart(Request $request){
    	// $value = Session::get('variableName');
    	// return $value;
    	// Session::has('cart')
    	$set_session_array = array(
    						'product_id'=>$request->product_id,
    						'product_size'=>$request->product_size,
    						'product_quantity'=>$request->product_quantity
    								);
    	if (Session::has('cart')){
    		$cart = Session::get('cart');
    		 array_push($cart,$set_session_array);
    		Session::put('cart',$cart);
    	}
    	else{
    		Session::put('cart',[$set_session_array]);
    	}
    	//$cart = Session::get('cart');
    	// return $cart;
    	// $cart = Session::forget('cart');

    	return back()->with('message','added to cart');
    }
    public function checkout(){
    // $cart = Session::forget('cart');
    $cart_data=[];
    $total_price=0;
    if (Session::has('cart')){
        $cart = Session::get('cart');
        foreach ($cart as $key => $item) {
            $get_product = Products::find($item['product_id']);
            $cart_data[] = array(
                                'product_id'=>$item['product_id'],
                                'product_image'=>$get_product->image_1,
                                'product_price'=>$get_product->price,
                                'product_quantity'=>$item['product_quantity'],
                                'sub_total'=>$get_product->price*$item['product_quantity']);
            $total_price = $total_price+$get_product->price*$item['product_quantity'];
            
        }
    }       
	return view('site.checkout',compact('cart_data','total_price'));
    }
    public function orderConfirmation(Request $request){
        $new_order = new Order();
        $new_order->title = $request->input('title');
        $new_order->product_arr = $request->input('order_history');
        $new_order->customer_name = $request->input('name');
        $new_order->email = $request->input('email');
        $new_order->phone = $request->input('phone');
        $new_order->address_1 = $request->input('address_1');
        $new_order->address_2 = $request->input('address_2');
        $new_order->state = $request->input('state');
        $new_order->zip = $request->input('zip');
        $new_order->country = $request->input('country');
        $new_order->payment_method = $request->input('payment_method');
        $new_order->voucher = $request->input('voucher');
        $new_order->save();
        $cart = Session::forget('cart');
        return back()->with('message','order confirmed successfully');
    }
    public function addProduct(){
        return view('admin.add-product');
    }
    public function addProductPost(Request $request){
        $new_product = new Products();
        $new_product->title = $request->input('title');
        $new_product->price = $request->input('price');
        $new_product->description = $request->input('description');
        $new_product->tagline = $request->input('tagline');
        if($request->hasFile('image_1')){
             $image_file1 = $request->file('image_1');
               $image_name =$image_file1->getClientOriginalName();
               $upPath = public_path().'/site/assets/images/products/'; 
               $image_file1->move($upPath, $image_name);
               $new_product->image_1 = $image_name;
          }
         if($request->hasFile('image_2')){
             $image_file2 = $request->file('image_2');
               $image_name2 = $image_file2->getClientOriginalName();
               $upPath = public_path().'/site/assets/images/products/'; 
               $image_file2->move($upPath, $image_name2);
               $new_product->image_2 = $image_name2;
          }
         if($request->hasFile('image_3')){
             $image_file3 = $image_file3->file('image_3');
               $image_name3 = $request->getClientOriginalName();
               $upPath = public_path().'/site/assets/images/products/'; 
               $image_file3->move($upPath, $image_name3);
               $new_product->image_3 = $image_name3;
          }
         if($request->hasFile('image_4')){
             $image_file4 = $image_file4->file('image_4');
               $image_name4 = $request->getClientOriginalName();
               $upPath = public_path().'/site/assets/images/products/'; 
               $image_file4->move($upPath, $image_name4);
               $new_product->image_4 = $image_name4;
          }
         if($request->hasFile('image_5')){
             $image_file5 = $request->file('image_5');
               $image_name5 = $image_file5->getClientOriginalName();
               $upPath = public_path().'/site/assets/images/products/'; 
               $image_file5->move($upPath, $image_name5);
               $new_product->image_5 = $image_name5;
          }
         if($request->hasFile('image_6')){
             $image_file6 = $request->file('image_6');
               $image_name6 = $image_file6->getClientOriginalName();
               $upPath = public_path().'/site/assets/images/products/'; 
               $image_file6->move($upPath, $image_name6);
               $new_product->image_6 = $image_name6;
          }
        $new_product->save();
        return back()->with('message','product added successfully');
    }
    public function viewProduct(){
        $all_products = Products::all();
        return view('admin.view-product',compact('all_products'));
    }
    public function addInventory(){
        $all_products = Products::all();
        return view('admin.add-inventory',compact('all_products'));
    }
    public function addInventoryPost(Request $request){
        $new_inventory = new Product_Inventory();
        $new_inventory->product_id = $request->input('product_id');
        $new_inventory->product_size = $request->input('product_size');
        $new_inventory->category = $request->input('product_category');
        $new_inventory->sub_category = $request->input('sub_category');
        $new_inventory->product_quantity = $request->input('quantity');
        $new_inventory->save();
        return back()->with('message','product Inventory added successfully');
    }
    public function deleteCart(Request $request){
      $id_product = $request->product_id;
      if (Session::has('cart')){
        $cart = Session::get('cart');
        foreach ($cart as $key => $item) {
           if($item['product_id']== $id_product){
              unset($cart[$key]);
           }
        }
        Session::put('cart',$cart);
      }
      else{
        return redirect()->route('cartCheckout');
      }
      return redirect()->route('cartCheckout');
    }
}
