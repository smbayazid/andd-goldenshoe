<?php

namespace App\Http\Controllers\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
    public function registerUser(Request $request){
    	$new_user = new User();
    	$new_user->name = $request->input('name');
    	$new_user->email = $request->input('email');
    	$new_user->gendar = $request->input('gendar');
    	$new_user->address = $request->input('address');
    	$new_user->password = bcrypt($request->input('password'));
    	$new_user->user_type = 2;
    	$new_user->save();
    	return back()->with('message','registration successful');
    	
    }
    // public function logoutUser(){
    // 	Auth::logout();
    //     // $request->session()->invalidate();
    //     // $request->session()->regenerateToken();
    //     return redirect('/');
    // }
    public function adminLogin(){
        return view('admin.admin-login');
    }
    public function adminLoginPost(Request $request){
        // $email = $request->input('email');
        // $password = $request->input('password');
        $credentials = $request->only('email', 'password');
        if(!Auth::attempt($credentials)) {
           $message = "Enter your correct email ID & Password";
            return redirect()->route('adminLogin')->with('successMessage', $message);
        }
        $user = Auth::getUser();
        if($user->user_type==1){
            return redirect()->route('addProduct');
        }
        else{
            $message = "User not an admin";
            return redirect()->route('adminLogin')->with('successMessage', $message);
        }
    }
    public function adminLogout(){
        Auth::logout();
        return view('admin.admin-login');
    }
}
