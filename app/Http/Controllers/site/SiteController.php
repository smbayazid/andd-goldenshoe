<?php

namespace App\Http\Controllers\site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Products;
use App\Product_Inventory;
use App\FeatureProduct;
use DB;
use Session;
class SiteController extends Controller
{
    public function index(){
        $all_products = Products::all();
        $feature_product_ids = FeatureProduct::all()->pluck('id');
        $feature_products = $all_products->whereIn('id',$feature_product_ids);
    	return view('site.index',compact('all_products','feature_products'));
    }
    public function about(){
    	return view('site.about');
    }
    public function blog(){
    	return view('site.blog');
    }
    public function testimonials(){
    	return view('site.testimonials');
    }
    public function terms(){
    	return view('site.terms');
    }
    public function contact(){
    	return view('site.contact');
    }
    
    public function blog_details(){
    	return view('site.blog-details');
    }
}
