@extends('site.common.template')
@section('body_content')
	<!-- Page Content -->
    <!-- Banner Starts Here -->

    <div class="heading-page header-text">
      <section class="page-heading">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="text-content">
                <h4>Checkout</h4>
                <h2>Lorem ipsum dolor sit amet.</h2>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
   
    <!-- Banner Ends Here -->
    <div class="container">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">No.</th>
            <th scope="col">Image</th>
            <th scope="col">Unit price</th>
            <th scope="col">Quantity</th>
            <th scope="col">sub total</th>
          </tr>
        </thead>
        <tbody>
          @php
            $order_history=[];
            $order_json=json_encode (new stdClass);;
          @endphp
          @foreach($cart_data as $key=>$cart_item)
            <tr>
              <th scope="row">{{$key+1}}</th>
              <td><img src="{{asset('site/assets/images/products/'.$cart_item['product_image'])}}" width="80" height="80"></td>
              <td>$<input style="border: none;outline: none;"  type="text" id="unitPrice{{$cart_item["product_id"]}}" name="" value="{{$cart_item['product_price']}}" readonly></td>
              <td>
                <button onclick='increment_function({{$cart_item["product_id"]}})' class="btn"><i class="fa fa-plus" aria-hidden="true"></i> </button> 
                <input style="width: 25px;text-align: center;" type="text" id="product_quantity{{$cart_item["product_id"]}}" name="product_quantity" value="{{$cart_item['product_quantity']}}" readonly>

                <button onclick="decrement_function({{$cart_item["product_id"]}})" class="btn"><i class="fa fa-minus" aria-hidden="true"></i></button> 
              </td>
              <td><p id="sub_total{{$cart_item["product_id"]}}">${{$cart_item['sub_total']}}</p> </td>
              <td><a href="{{route('deleteCart',$cart_item['product_id'])}}"><i style="color: red;font-size: 20px;" class="fa fa-trash-o" aria-hidden="true"></i></a></td>
            </tr>
            @php
              $order_history[]=array('product_id'=>$cart_item['product_id'],
                                      'product_quantity'=>$cart_item['product_quantity']
                                    );
              $order_json = json_encode($order_history);
            @endphp
          @endforeach
          <tr>
            <td>Total Price</td>
            <td colspan="3"></td>
            <td >${{$total_price}}</td>
          </tr>
          <hr>
        </tbody>
      </table>
      
    </div>
 @if(Session::has('message'))
      <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <section class="contact-us">
      <div class="container">
        <div class="inner-content">
          <div class="contact-us">
            <div class="contact-form">
                <form action="{{route('orderConfirmation')}}" method="post">
                   @csrf
                  <input type="hidden" name="order_history" value="{{$order_json}}">
                     <div class="row">
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Title:</label>
                                    <select class="form-control" data-msg-required="This field is required." name="title" required>
                                         <option value="">-- Choose --</option>
                                         <option value="dr">Dr.</option>
                                         <option value="miss">Miss</option>
                                         <option value="mr">Mr.</option>
                                         <option value="mrs">Mrs.</option>
                                         <option value="ms">Ms.</option>
                                         <option value="other">Other</option>
                                         <option value="prof">Prof.</option>
                                         <option value="rev">Rev.</option>
                                    </select>
                               </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Name:</label>
                                    <input type="text" class="form-control" name="name" required>
                               </div>
                          </div>
                     </div>
                     <div class="row">
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Email:</label>
                                    <input type="text" class="form-control" name="email" required>
                               </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Phone:</label>
                                    <input type="text" class="form-control" name="phone" required>
                               </div>
                          </div>
                     </div>
                     <div class="row">
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Address 1:</label>
                                    <input type="text" class="form-control" name="address_1" required>
                               </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Address 2:</label>
                                    <input type="text" class="form-control" name="address_2">
                               </div>
                          </div>
                     </div>
                     <div class="row">
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">City:</label>
                                    <input type="text" class="form-control" name="city" required>
                               </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">State:</label>
                                    <input type="text" class="form-control" name="state" required>
                               </div>
                          </div>
                     </div>
                     <div class="row">
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Zip:</label>
                                    <input type="text" class="form-control" name="zip" required>
                               </div>
                          </div>
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Country:</label>
                                    <select class="form-control" name="country" required>
                                         <option value="England">-- England --</option>
                                         <option value="Bangladesh">-- Bangladesh --</option>
                                         <option value="">-- Choose --</option>
                                         <option value="">-- Choose --</option>
                                    </select>
                               </div>
                          </div>
                     </div>

                     <div class="row">
                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">Payment method</label>

                                    <select class="form-control" name="payment_method" required>
                                         <option value="">-- Choose --</option>
                                         <option value="bank">Bank account</option>
                                         <option value="cash">Cash</option>
                                         <option value="paypal">PayPal</option>
                                    </select>
                               </div>
                          </div>

                          <div class="col-sm-6 col-xs-12">
                               <div class="form-group">
                                    <label class="control-label">voucher</label>
                                    <input type="text" class="form-control" name="voucher">
                               </div>
                          </div>
                     </div>

                     <div class="clearfix">
                          <!-- <button type="button" class="filled-button pull-left">Back</button> -->
                          
                          <button type="submit" class="filled-button pull-right">Finish</button>
                     </div>
                </form>
            </div>
          </div>
        </div>
      </div>
    </section>
@endsection