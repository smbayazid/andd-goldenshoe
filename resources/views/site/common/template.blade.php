<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

    <title>Golden Shoe</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('site/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{asset('site/assets/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/owl.css')}}">
  </head>

  <body>
     <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
    <!-- ***** Preloader End ***** -->
    @include('site.common.header')
    @yield('body_content')
    @include('site.common.footer')

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('site/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('site/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Additional Scripts -->
    <script src="{{asset('site/assets/js/custom.js')}}"></script>
    <script src="{{asset('site/assets/js/owl.js')}}"></script>
    <script src="{{asset('site/assets/js/slick.js')}}"></script>
    <script src="{{asset('site/assets/js/isotope.js')}}"></script>
    <script src="{{asset('site/assets/js/accordions.js')}}"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }

    </script>
    <script type="text/javascript">
      function increment_function(product_id){
        var quantity = $('#product_quantity'+product_id).val();
        quantity = parseInt(quantity)+1;
        var unitPrice = $('#unitPrice'+product_id).val();
        sub_total = parseInt(unitPrice) * parseInt(quantity);
        $('#product_quantity'+product_id).val(quantity);
        $('#sub_total'+product_id).text('$'+sub_total);
      }
      function decrement_function(product_id){
        var quantity = $('#product_quantity'+product_id).val();
        if(parseInt(quantity)>1){
          quantity = parseInt(quantity)-1;
          var unitPrice = $('#unitPrice'+product_id).val();
          sub_total = parseInt(unitPrice) * parseInt(quantity);
          $('#product_quantity'+product_id).val(quantity);
          $('#sub_total'+product_id).text('$'+sub_total);
        }
        
      }
      // $( "#increment_btn" ).click(function() {
      //   alert( "Handler for .click() called." );
      // });
      //  $( "#increment_btn" ).click(function() {
      //   alert( "Handler for .click() called." );
      // });
    </script>

  </body>
</html>