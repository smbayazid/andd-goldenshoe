    <!-- Header -->

    <header class="">
      <nav class="navbar  navbar-expand-lg " >
        <div class="container">
          <a class="navbar-brand " href="{{url('/')}}" ><img style="width: 160px; height: auto;" src="{{asset('site/assets/images/logo/logo22.jpg')}}"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item {{request()->is('/') ? 'active' : '' }} ">
                <a class="nav-link " href="{{url('/')}}">Home
                  <span class="sr-only">(current)</span>
                </a>
              </li> 
              <li class="nav-item {{request()->is('products') ? 'active' : '' }}">
                <a class="nav-link" href="{{url('/products')}}">Products</a>
              </li>

              <!-- <li class="nav-item {{request()->is('checkout') ? 'active' : '' }}">
                <a class="nav-link" href="{{url('/checkout')}}">Checkout</a>
              </li> -->

              <li class="nav-item dropdown ">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">About</a>
                  
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{url('/about')}}">About Us</a>
                    <a class="dropdown-item" href="{{url('/blog')}}">Blog</a>
                    <a class="dropdown-item" href="{{url('/testimonials')}}">Testimonials</a>
                    <a class="dropdown-item" href="{{url('/terms')}}">Terms</a>
                  </div>
              </li>
              <li class="nav-item {{request()->is('contact') ? 'active': '' }}">
                <a class="nav-link" href="{{url('/contact')}}">Contact Us</a>
              </li>
               <li class="nav-item {{request()->is('checkout') ? 'active': '' }}">
                <a class="nav-link" href="{{url('/checkout')}}"><i style="font-size: 20px;" class="fa fa-cart-plus" aria-hidden="true"></i></a>
              </li>
              @guest
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i style="font-size: 20px;" class="fa fa-user" aria-hidden="true"></i></a>
                  
                  <div class="dropdown-menu">
                    <a class="dropdown-item" data-toggle="modal" data-target="#exampleModal" >Login</a>
                    <a class="dropdown-item" href="#">Live Support</a>
                    
                  </div>
                <!-- <a class="nav-link" href="{{ route('login') }}"><i style="font-size: 20px;" class="fa fa-user" aria-hidden="true"></i></a> -->
              </li>
               @else
                <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    <!-- {{ Auth::user()->name }} -->
                                    <i style="font-size: 20px;" class="fa fa-user" aria-hidden="true"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                     <a href="" class="dropdown-item">Order History</a>
                                      <a class="dropdown-item" href="#">Live Support</a>
                                </div>
                            </li>
              @endguest
            </ul>
          </div>
        </div>
      </nav>
    </header>
  <!-- Modal -->
<div class="modal  fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
    <!--   <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div> -->
      <div class="modal-body">
        <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Login') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
      </div>
    <div class="modal-footer">
      <a href="{{ route('register') }}" class="btn btn-secondary mr-auto"> Sign Up</a>
      <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
      <span>Easy Sign Up With </span>
      <a href="#"><i style="font-size: 20px; margin-left: 10px; margin-right: 10px;" class="fa fa-facebook" aria-hidden="true"></i></a>
      <a href="#"><i style="font-size: 20px; margin-left: 10px; margin-right: 10px;color:   #db4a39;" class="fa fa-google" aria-hidden="true"></i></a> 
      <a href="#"><i style="font-size: 20px; margin-left: 10px; margin-right: 10px;color:  #3f729b;" class="fa fa-instagram" aria-hidden="true"></i></a>
      
    </div>
  </div>
  </div>
</div>
<!-- end login modal -->