@extends('admin.template')

@section('admin_content')
<div class="container">
	<table class="table">
  <thead>
    <tr>
      <!-- <th scope="col">SN</th> -->
      <th scope="col">Image</th>
      <th scope="col">Title</th>
      <th scope="col">Tagline</th>
      <th scope="col">Price</th>
      <th scope="col">Action</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($all_products as $key=>$product)
    <tr>
      <!-- <th scope="row">{{$key+1}}</th> -->
      <td><img height="50" width="50" src="{{asset('site/assets/images/products/'.$product->image_1)}}"></td>
      <td>{{$product->title}}</td>
      <td>{{$product->tagline}}</td>
      <td>${{$product->price}}</td>
      <td><a href="" class="btn btn-info">Edit</a></td>
      <td><a href="" class="btn btn-danger">Delete</a></td>
    </tr>
    @endforeach
    <!-- <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>Larry</td>
      <td>the Bird</td>
      <td>@twitter</td>
    </tr> -->
  </tbody>
</table>
</div>
@endsection