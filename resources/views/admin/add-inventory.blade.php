@extends('admin.template')

@section('admin_content')

<div class="container ">
	 @if(Session::has('message'))
      <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
	<form class="p-4" action="{{route('addInventoryPost')}}" method="post" enctype="multipart/form-data">
		@csrf
	  <div class="form-group">
	    <label for="product_id">Product</label>
	    <select  class="form-control" id="product_id" name="product_id">
	      <option value="">select product</option>
	      @foreach($all_products as $product)
	      	<option value="{{$product->id}}">{{$product->title}}</option>
	      @endforeach
	    </select>
	  </div>
	  <div class="form-group">
	    <label for="product_size">Product Size</label>
	    <input type="text" class="form-control" id="product_size" name="product_size" aria-describedby="product_size" placeholder="Enter product size">
	  </div>
	   <div class="form-group">
	    <label for="product_category">Product Category</label>
	    <select  class="form-control" id="product_category" name="product_category">
	      <option value="">select category</option>
	      <option value="men">Men</option>
	      <option value="women">Women</option>
	      <option value="kid">Kid</option>
	    </select>
	  </div>
	   <div class="form-group">
	    <label for="sub_category">Product Sub Category</label>
	    <input type="text" class="form-control" id="sub_category" name="sub_category" aria-describedby="sub_category" placeholder="Enter product sub category">
	  </div>
	  <div class="form-group">
	    <label for="quantity">Product Quantity</label>
	    <input type="number" class="form-control" id="quantity" name="quantity" aria-describedby="quantity" placeholder="Enter quantity">
	  </div>
	  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
</div>
@endsection