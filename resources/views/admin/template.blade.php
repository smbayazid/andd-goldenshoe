<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">

    <title>Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('site/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">


    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="{{asset('site/assets/css/fontawesome.css')}}">
    <link rel="stylesheet" href="{{asset('admin/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('site/assets/css/owl.css')}}">
  </head>

  <body>
     <!-- ***** Preloader Start ***** -->
    <div id="preloader">
        <div class="jumper">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>  
     <div id="page-content-wrapper" >

      <nav class="navbar navbar-expand-lg navbar-light bg-dark ">
        <a href="{{url('/')}}"><img class="mt-2 ml-5" height="auto" width="150" src="{{asset('site/assets/images/logo/logo22.jpg')}}"></a>
        
      </nav>
     
     </div>
     <div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class=" border-right sidebar-bg" id="sidebar-wrapper" >
      <!-- <div class="sidebar-heading">Start Bootstrap </div> -->
      <ul class="list-group list-group-flush" >
    <!--   <li class="bottom-red-border">
        <a href="" class="list-group-item  text-color menu-a-bg" >
        <i class="fas fa-home"></i> Dashboard <i  class="fas fa-caret-right"></i></a> 
        </li> -->

       <li class="bottom-red-border border-bottom">
        <a href="{{route('addProduct')}}" class="list-group-item  text-dark"> Add Product</a>
      </li >


     <!--  <li class="bottom-red-border">
        <a href="#" class="list-group-item  text-color menu-a-bg"><i class="fas fa-heading"></i> Display Host</a>
      </li > -->
       <li class="bottom-red-border border-bottom">
        <a href="{{route('viewProduct')}}" class="list-group-item  text-dark"> View Product</a>
      </li >
      <li class="bottom-red-border border-bottom">
        <a href="{{route('addInventory')}}" class="list-group-item  text-dark"> Add Inventory</a>
      </li>

      <li class="bottom-red-border border-bottom">
        <a href="" class="list-group-item  text-dark"> View Inventory</a>
      </li >
      <li class="bottom-red-border border-bottom">
        <a href="{{route('adminLogout')}}" class="list-group-item  text-dark"> Logout</a>
      </li >

      </ul>
    </div>
    <!-- ***** Preloader End ***** -->
    @yield('admin_content')

    <!-- Bootstrap core JavaScript -->
    <script src="{{asset('site/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('site/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Additional Scripts -->
    <script src="{{asset('site/assets/js/custom.js')}}"></script>
    <script src="{{asset('site/assets/js/owl.js')}}"></script>
    <script src="{{asset('site/assets/js/slick.js')}}"></script>
    <script src="{{asset('site/assets/js/isotope.js')}}"></script>
    <script src="{{asset('site/assets/js/accordions.js')}}"></script>

    <script language = "text/Javascript"> 
      cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
      function clearField(t){                   //declaring the array outside of the
      if(! cleared[t.id]){                      // function makes it static and global
          cleared[t.id] = 1;  // you could use true and false, but that's more typing
          t.value='';         // with more chance of typos
          t.style.color='#fff';
          }
      }

    </script>

  </body>
</html>