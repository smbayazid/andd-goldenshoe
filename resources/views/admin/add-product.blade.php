@extends('admin.template')

@section('admin_content')

<div class="container ">
	 @if(Session::has('message'))
      <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
	<form class="p-4" action="{{route('addProductPost')}}" method="post" enctype="multipart/form-data">
		@csrf
	  <div class="form-group">
	    <label for="title">Product Title</label>
	    <input type="text" class="form-control" id="title" name="title" aria-describedby="title" placeholder="Enter Title">
	  </div>
	  <div class="form-group">
	    <label for="tagline">Product Tagline</label>
	    <input type="text" class="form-control" id="tagline" name="tagline" aria-describedby="tagline" placeholder="Enter tagline">
	  </div>
	  <div class="form-group">
	    <label for="title">Product Price</label>
	    <input type="number" class="form-control" id="price" name="price" aria-describedby="price" placeholder="Enter price">
	  </div>
	  <div class="form-group">
	    <label for="description">Product Description</label>
	    <textarea class="form-control" id="description" name="description" rows="3"></textarea>
	  </div>
	  <div class="form-group">
	    <label for="image_1">Image 1</label>
	    <input type="file" class="form-control-file" id="image_1" name="image_1">
	  </div>
	   <div class="form-group">
	    <label for="image_2">Image 2</label>
	    <input type="file" class="form-control-file" id="image_2" name="image_2">
	  </div>
	  <div class="form-group">
	    <label for="image_3">Image 3</label>
	    <input type="file" class="form-control-file" id="image_3" name="image_3">
	  </div>
	  <div class="form-group">
	    <label for="image_4">Image 4</label>
	    <input type="file" class="form-control-file" id="image_4" name="image_4">
	  </div>
	  <div class="form-group">
	    <label for="image_5">Image 5</label>
	    <input type="file" class="form-control-file" id="image_5" name="image_5">
	  </div>
	  <div class="form-group">
	    <label for="image_6">Image 6</label>
	    <input type="file" class="form-control-file" id="image_6" name="image_6">
	  </div>
	  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
</div>
@endsection