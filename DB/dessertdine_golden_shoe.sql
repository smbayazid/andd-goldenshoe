-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 29, 2021 at 11:35 AM
-- Server version: 10.3.29-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dessertdine_golden_shoe`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feature_products`
--

CREATE TABLE `feature_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `feature_products`
--

INSERT INTO `feature_products` (`id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(8, '2021_06_19_021718_create_products_table', 1),
(9, '2021_06_19_022640_create_product__inventories_table', 1),
(10, '2021_06_19_022719_create_orders_table', 1),
(12, '2021_06_19_043522_tagline_to_products_table', 2),
(13, '2014_10_12_100000_create_password_resets_table', 3),
(14, '2021_06_21_031837_create_feature_products_table', 3),
(15, '2021_06_21_140520_add_fields_to_orders_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_arr` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `voucher` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `created_at`, `updated_at`, `title`, `product_arr`, `customer_name`, `customer_id`, `email`, `phone`, `address_1`, `address_2`, `state`, `zip`, `country`, `payment_method`, `voucher`) VALUES
(5, '2021-06-28 23:34:24', '2021-06-28 23:34:24', 'mr', '[{\"product_id\":\"2\",\"product_quantity\":\"1\"}]', 'User', NULL, 'user@goldenshoe.com', '11223344', 'n/a', 'n/a', 'n/a', '1122', 'Bangladesh', 'bank', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double NOT NULL,
  `image_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_4` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_5` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_6` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tagline` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `description`, `price`, `image_1`, `image_2`, `image_3`, `image_4`, `image_5`, `image_6`, `created_at`, `updated_at`, `tagline`) VALUES
(1, 'VATTAL - Leather Derby shoes', 'A stylish addition to any footwear collection, Ted’s latest offering is sure to make ensembles more dapper all around. Made in leather, the VATTAL shoes are uniquely finished with a colour detail on the heel.\r\nDetails:\r\n    Ted Baker footwear collection\r\n    Leather\r\n    Derby style\r\n    Lace up\r\n    ‘T’ detail on heel\r\n    Contrast colour on heel\r\n    Ted Baker-branded\r\nCare & Fabric:\r\n    Fabric Content: Upper: 100% Bovine leather; Lining: 75% Cotton, 25% Polyurethane; Sock: 50% Polyurethane, 50% Cotton; Sole: 100% Rubber\r\n    Care information: Do not wash, iron or dry clean. Entrust to specialist leather cleaners only.', 190, 'product_1_img_1.jpg', 'product_1_img_2.jpg', 'product_1_img_3.jpg', 'product_1_img_4.jpg', 'product_1_img_5.jpg', NULL, NULL, NULL, 'A stylish addition to any footwear collection.'),
(2, 'Merino Runners MEN', 'Our men’s merino runners are designed for maximum comfort, grip and performance. Made of Merino Wool and featuring a unique 3D Stretch technology, the men’s Giesswein Runners are hyper lightweight, flexible and very durable.\r\nThe natural properties of merino wool ensure your feet are warm in Winter, and cool in Summer, providing perfect temperature control. The naturally soft merino wool fibres mean you won’t itch or scratch. Naturally odor resistant, you can run all day and not worry about a smell.\r\nWHY MERINO RUNNERS?\r\nANTIBACTERIAL\r\nODOR FREE\r\nDRY SKIN\r\nHEAT CONTROL\r\n', 220, 'product_2_img_1.jpg', 'product_2_img_2.jpg', 'product_2_img_3.jpg', '', '', '', NULL, NULL, 'Our men’s merino runners are designed for maximum comfort'),
(3, 'Christian Louboutin', 'Inspired by the designer’s favourite Parisian neighbourhood, renowned for its vibrant nightlife.\r\nThe Pigalle pumps are to Christian Louboutin what a sultry red lip is to the confident woman: always impactful, no matter the guise it takes. Here, the Maison’s iconic shoe is made all the more powerful with an upper crafted from glossy patent leather, its reflective shine perfectly complementing the silhouette’s signature pointed toe, stiletto heel and red lacquered sole – a sought-after feature among royalty, A-list celebrities and industry insiders.\r\nPlease note, Christian Louboutin’s signature lacquered soles will wear out with use; this is normal wear and tear and is not considered to be a manufacturing defect. Please try these shoes on a carpeted surface, as any damage to the soles may prevent us from accepting returns.\r\n', 299, 'Product3-1.jpg', 'Product3-2.jpg', 'Product3-3.jpg', 'Product3-4.jpg', 'Product3-5.jpg', 'Product3-6.jpg', NULL, NULL, 'Inspired by the designer’s favourite Parisian neighbourhood'),
(5, 'Christian Louboutin - Brown Soft Leather', 'A signature flash of red is the perfect ode to Christian Louboutin’s rule-bending inspirations.\r\nThe smooth, sensual lines of the Christian Louboutin Iriza pumps may be softly spoken, but the designer’s inspiration for creating shoes like these is one of pure rebellion. After a visit to Paris’ National Museum of African and Oceania Arts in 1976 and spotting a sign that forbade women from wearing heels – claiming they damaged the building’s wooden flooring – he endeavoured to manifest footwear that would break the rules and, most importantly, empower. The red lacquered sole and sky-high stiletto heel seen here are undoubtedly an ode to this.\r\nPlease note, Christian Louboutin’s signature lacquered soles will wear out with use; this is normal wear and tear and is not considered to be a manufacturing defect. Please try these shoes on a carpeted surface, as any damage to the soles may prevent us from accepting returns.\r\n', 499, 'Product4-1.jpg', 'Product4-2.jpg', 'Product4-3.jpg', 'Product4-4.jpg', 'Product4-5.jpg', 'Product4-6.jpg', NULL, NULL, 'A signature flash of red is the perfect ode to Christian Louboutin’s rule-bending inspirations.'),
(6, 'Jimmy Choo', 'A towering stiletto heel amplifies the playfulness of this design.\r\nJimmy Choo brings together artful creativity and fresh simplicity to create the Bay sandals. Crafted in Italy using tubular pieces of leather that knot together to create a sculptural silhouette, it\'s finished with a square toe and a curved stiletto heel for a striking look.\r\n', 599, 'Product5-1.jpg', 'Product5-2.jpg', 'Product5-3.jpg', NULL, NULL, NULL, NULL, NULL, 'A towering stiletto heel amplifies the playfulness of this design.\r\n'),
(7, 'Mini Melissa', 'The glittery outer is the epitome of party-ready.\r\nBrazilian footwear label Mini Melissa is known for its reinventions of the classic jelly shoe – in the case of the Ultragirl ballet flats, the flexible material comes moulded into a delicate open-toe pump silhouette. This pair is adorned with three-dimensional butterflies perched on the toes, providing the magic that any little girl dreams of.', 480, 'Product6-1.jpg', 'Product6-2.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 'The glittery outer is the epitome of party-ready.\r\nBrazilian footwear label Mini Melissa is known for its reinventions of the classic jelly shoe – in the case of the Ultragirl ballet flats'),
(8, 'test', 'testing', 234, 'Capture.PNG', NULL, NULL, NULL, NULL, NULL, '2021-06-21 20:50:30', '2021-06-21 20:50:30', 'Testing');

-- --------------------------------------------------------

--
-- Table structure for table `product__inventories`
--

CREATE TABLE `product__inventories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product__inventories`
--

INSERT INTO `product__inventories` (`id`, `product_id`, `product_size`, `category`, `sub_category`, `product_quantity`, `created_at`, `updated_at`) VALUES
(1, 1, '19', 'men', 'shoes', 3, NULL, NULL),
(2, 1, '36', 'women', 'shoes', 10, NULL, NULL),
(3, 1, '22', 'kid', 'shoes', 20, NULL, NULL),
(4, 2, '33', 'men', 'sports', 10, NULL, NULL),
(5, 3, '33', 'women', 'shoes', 10, '2021-06-21 21:52:38', '2021-06-21 21:52:38');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gendar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `gendar`, `dob`, `address`, `email_verified_at`, `password`, `user_type`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'sufi', 'sufi@baba.com', 'Male', NULL, 'Dhaka, Bangladesh', NULL, '$2y$10$t3CH.Y82jZElSRjpiGg82uTSpf7gqSRGFLR.8Msi35J9jP.gdXpKC', '1', NULL, '2021-06-20 21:00:37', '2021-06-20 21:00:37'),
(2, 'bayazid', 'baba@sufi.om', 'Male', NULL, 'London, UK', NULL, '$2y$10$83jYQM4wAuAl16IICYU95OiU3hHCfZAb2fmkQrKpuHeVAXTBvn24.', '2', NULL, '2021-06-20 21:14:23', '2021-06-20 21:14:23'),
(3, 'Goldenshoe Admin', 'admin@goldenshoe.com', 'Other', NULL, 'Golden Shoe', NULL, '$2y$10$ZJgjbL05x5cSSjrmEqnMnOlAiJYrSpfvKq7TXtdZY5ybMVsLdy0YS', '1', NULL, '2021-06-28 23:28:45', '2021-06-28 23:28:45'),
(4, 'Goldenshoe User', 'user@goldenshoe.com', 'Male', NULL, 'Golden Shoe', NULL, '$2y$10$KgmRhO7CVtBXP.oQmMbSe.VNh93IiwybJ5Nu0C4y1dr49S4.WDGHC', '2', NULL, '2021-06-28 23:29:31', '2021-06-28 23:29:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_products`
--
ALTER TABLE `feature_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product__inventories`
--
ALTER TABLE `product__inventories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feature_products`
--
ALTER TABLE `feature_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product__inventories`
--
ALTER TABLE `product__inventories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
