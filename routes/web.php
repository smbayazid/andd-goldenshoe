<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/','site\SiteController@index');
Route::get('/products','site\ProductController@products');
Route::get('/checkout','site\ProductController@checkout')->name('cartCheckout');
Route::get('/about','site\SiteController@about');
Route::get('/blog','site\SiteController@blog');
Route::get('/testimonials','site\SiteController@testimonials');
Route::get('/terms','site\SiteController@terms');
Route::get('/contact','site\SiteController@contact');
Route::get('/product-details/{product_id}','site\ProductController@product_details');
Route::get('/blog-details','site\SiteController@blog_details');
Route::post('/add-to-cart','site\ProductController@addToCart');
Route::post('/register-user','site\AuthController@registerUser')->name('user_registration');
Route::post('/order-confirmation','site\ProductController@orderConfirmation')->name('orderConfirmation');
// Route::get('/logout-user','site\AuthController@logoutUser')->name('user_logout');
Auth::routes();
Route::get('/add-product','site\ProductController@addProduct')->name('addProduct');
Route::get('/add-inventory','site\ProductController@addInventory')->name('addInventory');
Route::get('/view-product','site\ProductController@viewProduct')->name('viewProduct');
Route::post('/add-product-post','site\ProductController@addProductPost')->name('addProductPost');
Route::post('/add-inventory-post','site\ProductController@addInventoryPost')->name('addInventoryPost');
Route::get('/admin-login','site\AuthController@adminLogin')->name('adminLogin');
Route::post('/admin-login-post','site\AuthController@adminLoginPost')->name('adminLoginPost');
Route::get('/admin-logout','site\AuthController@adminLogout')->name('adminLogout');
Route::get('/delete-cart/{product_id}','site\ProductController@deleteCart')->name('deleteCart');
// Route::get('/home', 'HomeController@index')->name('home');

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
